<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('home');
Route::get('/product/{id}', 'HomeController@product_detail');

Route::group(["prefix" => "dashboard"], function(){
    Route::get('/', 'DashboardController@index');    

    // users controller
    Route::resource('users', 'UsersController');
    Route::get('/users/delete/{id}', 'UsersController@destroy');

    // user_types controller
    Route::resource('/user_types', 'UserTypesController');
    Route::get('/user_types/delete/{id}', 'UserTypesController@destroy');

    // products controller
    Route::resource('/products', 'ProductsController');
    Route::get('/products/delete/{id}', 'ProductsController@destroy');
    Route::get('/products/change_status/{id}', 'ProductsController@change_status');
    Route::get('/products/create_image/{id}', 'ProductsController@create_image'); 
    Route::post('/products/store_image', 'ProductsController@store_image'); 
    Route::get('/products/delete_product_image/{id}', 'ProductsController@delete_product_image');     

    // product_types controller
    Route::resource('/product_types', 'ProductTypesController');
    Route::get('/product_types/delete/{id}', 'ProductTypesController@destroy');
});