@extends('layouts.dashboard')

@section('content')
<!-- Dashboard -->
<div class="dashboard-container">
    <div class="row">
        <div class="col-xl-3 col-lg-6 col-sm-6 col-xs-12">
            <div class="widget widget-seven background-success">
                <div class="widget-body">
                    <div href="javascript: void(0);" class="widget-body-inner">
                        <h5 class="text-uppercase">Week Sales</h5>
                        <i class="counter-icon icmn-cash3"></i>
                        <span class="counter-count">
                            <i class="icmn-arrow-up5"></i>
                            $<span class="counter-init" data-from="25" data-to="942"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-sm-6 col-xs-12">
            <div class="widget widget-seven background-default">
                <div class="widget-body">
                    <div href="javascript: void(0);" class="widget-body-inner">
                        <h5 class="text-uppercase">Server Uptime</h5>
                        <i class="counter-icon icmn-server"></i>
                        <span class="counter-count">
                            <i class="icmn-arrow-down5"></i>
                            <span class="counter-init" data-from="0" data-to="99"></span>%
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-sm-6 col-xs-12">
            <div class="widget widget-seven">
                <div class="widget-body">
                    <div href="javascript: void(0);" class="widget-body-inner">
                        <h5 class="text-uppercase">New Clients</h5>
                        <i class="counter-icon icmn-users"></i>
                        <span class="counter-count">
                            <i class="icmn-arrow-up5"></i>
                            <span class="counter-init" data-from="0" data-to="67"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-sm-6 col-xs-12">
            <div class="widget widget-seven">
                <div class="widget-body">
                    <div href="javascript: void(0);" class="widget-body-inner">
                        <h5 class="text-uppercase">Subscriptions</h5>
                        <i class="counter-icon icmn-users"></i>
                        <span class="counter-count">
                            <i class="icmn-arrow-up5"></i>
                            <span class="counter-init" data-from="0" data-to="356"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
</div>

<!-- End Dashboard -->
@endsection