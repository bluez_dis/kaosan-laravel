<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse">
                <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Lembayung</a>
        </div>
        <!-- Menu -->                
        <div class="collapse navbar-collapse" id="custom-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Home</a>   </li>    
                <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Shop</a>
                <ul class="dropdown-menu" role="menu">
                    <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Product</a>
                    <!-- <ul class="dropdown-menu">
                        <li><a href="shop_product_col_3.html">3 columns</a></li>
                        <li><a href="shop_product_col_4.html">4 columns</a></li>
                    </ul> -->
                    </li>
                    <li><a href="shop_single_product.html">Single Product</a></li>
                    <li><a href="shop_checkout.html">Checkout</a></li>
                </ul>
                </li>     
            </ul>
        </div>
        <!-- menu -->
    </div>
</nav>

<section class="home-section home-fade home-full-height" id="home">
    <div class="hero-slider">
        <ul class="slides">
        <li class="bg-dark-30 bg-dark shop-page-header" style="background-image:url(&quot;assets/images/shop/slider1.png&quot;);">
            <div class="titan-caption">
            <div class="caption-content">
                <!-- <div class="font-alt mb-30 titan-title-size-1">This is</div> -->
                <img src="/image/lembayung2.png" size="50%" alt="lembayung" />
                <div class="font-alt mb-30 titan-title-size-4"> 2021</div>
                <div class="font-alt mb-40 titan-title-size-1">Your online fashion destination</div><a class="section-scroll btn btn-border-w btn-round" href="#latest">Learn More</a>
            </div>
            </div>
        </li>
        <li class="bg-dark-30 bg-dark shop-page-header" style="background-image:url(&quot;assets/images/shop/slider3.png&quot;);">
            <div class="titan-caption">
            <div class="caption-content">
                <div class="font-alt mb-30 titan-title-size-1"> This is Lembayung</div>
                <div class="font-alt mb-40 titan-title-size-4">Exclusive products</div><a class="section-scroll btn btn-border-w btn-round" href="#latest">Learn More</a>
            </div>
            </div>
        </li>
        </ul>
    </div>
</section>