<ul class="left-menu-list left-menu-list-root list-unstyled">        
    <li class="left-menu-list-{{ (request()->is('/')) ? 'active' : '' }}">
        <a class="left-menu-link" href="/">
            <i class="left-menu-link-icon icmn-home2"></i>
            <span class="menu-top-hidden">Dashboard</span>
        </a>
    </li>
    <li class="left-menu-list-separator "></li>    
    
    <li class="left-menu-list-{{ (request()->is('meetings')) ? 'active' : '' }}">
        <a class="left-menu-link" href="/dashboard/products">
            <i class="left-menu-link-icon icmn-user-plus2"></i>
            <span class="menu-top-hidden">Data Produk</span>
        </a>
    </li>
    <li class="left-menu-list-submenu">
        <a class="left-menu-link" href="javascript: void(0);">
            <i class="left-menu-link-icon icmn-notebook util-spin-delayed-pseudo"></i>
            Laporan
        </a>                         
        <ul class="left-menu-list list-unstyled">
            <li>
                <a class="left-menu-link" href="/reports_transactions">
                    <i class="left-menu-link-icon"></i>
                    Penjualan
                </a>                            
                <a class="left-menu-link" href="/reports_purchases">
                    <i class="left-menu-link-icon"></i>
                    Pembelian
                </a>                            
            </li>                    
        </ul>                               
    </li> 
    
    <li class="left-menu-list-separator "></li>    
    <li class="left-menu-list-submenu">
        <a class="left-menu-link" href="javascript: void(0);">
            <i class="left-menu-link-icon icmn-cog util-spin-delayed-pseudo"></i>
            Setting Page
        </a>
        <ul class="left-menu-list list-unstyled">
            <li>
                <a class="left-menu-link" href="/dashboard/users">
                    <i class="counter-icon icmn-user"></i>
                    Mengelola User
                </a>
            </li>
            <li>
                <a class="left-menu-link" href="/dashboard/user_types">
                    <i class="counter-icon icmn-users"></i>
                    Mengelola Tipe User 
                </a>
            </li>
            <li>
                <a class="left-menu-link" href="/dashboard/product_types">
                    <i class="counter-icon icmn-users"></i>
                    Mengelola Tipe Product 
                </a>
            </li>                        
        </ul>   
    </li>
    
</ul>