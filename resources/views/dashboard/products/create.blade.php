@extends('layouts.dashboard')

@section('content')
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Form Data User</h3>
    </div>
    <br />
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/dashboard/products') }}" enctype="multipart/form-data">
    <div class="panel-body">
        <div class="row">        
            <div class="col-lg-12">
                <div class="margin-bottom-50">                    
                    <br />
                    <!-- Horizontal Form -->                                           
                    <input type="hidden" class="form-control" name="created_by" id="created_by" value="{{ Auth::user()->id }}">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Nama</label>
                        </div>
                        <div class="col-md-10">
                            <input id="name" type="text" class="form-control" name="name"  placeholder="Input nama user" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Produk Tipe</label>
                        </div>
                        <div class="col-md-10">
                            <select id="product_type_id" type="text" class="form-control select2" name="product_type_id">
                                <option value="">--- Pilih Produk Tipe ---</option>
                                @foreach ($product_types as $product_type)
                                    <option value="{{ $product_type->id }}">{{$product_type->name }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('product_type_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('product_type_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Gambar</label>
                        </div>
                        <div class="col-md-10">
                            <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image" data-max-size="2097152"  placeholder="Input Gambar Sidang" autofocus>

                            @if ($errors->has('image'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>                    
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Harga</label>
                        </div>
                        <div class="col-md-10">
                            <input id="price" type="text" class="form-control" name="price"  placeholder="Input harga produk" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Deskripsi</label>
                        </div>
                        <div class="col-md-10">
                            <textarea id="description" name="description" class="form-control summernote" rows="4" cols="50" autofocus>
                                Input deskripsi produk
                            </textarea>                            
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Jumlah</label>
                        </div>
                        <div class="col-md-10">
                            <input id="quantity" type="text" class="form-control" name="quantity"  placeholder="Input Jumlah produk" autofocus>
                        </div>
                    </div>  
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">link Tokped</label>
                        </div>
                        <div class="col-md-10">
                            <input id="link_tokped" type="text" class="form-control" name="link_tokped"  placeholder="Input Link tokped produk" autofocus>
                        </div>
                    </div>                     
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">link Shope</label>
                        </div>
                        <div class="col-md-10">
                            <input id="link" type="text" class="form-control" name="link_shope" placeholder="Input Link tokped produk" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">link Lazada</label>
                        </div>
                        <div class="col-md-10">
                            <input id="link" type="text" class="form-control" name="link_lazada" placeholder="Input Link tokped produk" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">link Bukalapak</label>
                        </div>
                        <div class="col-md-10">
                            <input id="link" type="text" class="form-control" name="link_bukalapak" placeholder="Input Link tokped produk" autofocus>
                        </div>
                    </div>                   
                                                                                                    
                </div>
            </div>
        </div>

        <br>
        <h6>Foto Produk<h6>
        <div class="form-group row">
            <div id="dinamic_form">
                <div id="row_0">              
                    <div class="box-body">
                        <div class="row">                
                        <div class="col-xs-3">
                            <a id="add" class="btn btn-success btn-sm"> <i class="fa fa-plus"></i></a>
                            <a id="remove" class="btn btn-danger btn-sm"><i class="fa fa-remove"></i></a>
                        </div>
                        <div class="col-xs-3">
                            <input type="file" class="form-control" id="document_0" name="document[0]" data-max-size="2097152" placeholder="Upload File " required="">                                            
                        </div>                                                                                                
                        <div class="col-xs-3">
                            <p class="text-red">* Ukuran Maksimal File 2MB</p>
                            <p class="text-red">* Format File : pdf, dokumen, rar, zip</p>
                            <p class="text-red">* Hapus field jika tidak di gunakan</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" class="form-control" name="count_looping_product_image" id="count_looping_product_image" value="1">

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Create') }}
                </button>
                <a class="btn btn-success" href="/dashboard/users/">Back</a>
            </div>
        </div>
    </div>
    </form>
    <!-- End Horizontal Form -->      
</section>
<!-- End -->
<script>
    $(function(){
        $('.select2').select2();

        $('.summernote').summernote({
            height: 350
        });

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD'
        });

        var i = 1;
        $('#add').click(function(){  
            $('#dinamic_form').append('<div id="row_'+i+'"><div class="box-body"><div class="row"><div class="col-xs-3"></div><div class="col-xs-3"><input type="file" class="form-control" id="document['+i+']" name="document['+i+']" placeholder="Upload File " required=""></div><div class="col-xs-2"></div></div></div></br>');
            ++i;
            $('#count_looping_product_image').val(i);
        });

        $('#remove').click(function(){
            if(i != 0){
                --i;
            }
            $('#count_looping_product_image').val(i);
            $('#row_'+i).remove();               
        });

        var image = $('#image');        
        var maxSize = image.data('max-size');
        var document = $('#document_0');
        $("form").submit(function(){
            if(image.val() != '' && image.get(0).files[0].size>maxSize){
                alert('file gambar sidang melebihi batas ' +maxSize+ ' bytes');
                return false;
            }else if(document.val() != '' && document.get(0).files[0].size>maxSize){
                alert('file dokumen repot melebihi batas ' +maxSize+ ' bytes');
                return false;
            }      
        });
    })
</script>
@endsection