@extends('layouts.dashboard')

@section('content')
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Form Data Post</h3>
    </div>
    <br />
    <form class="form-horizontal" role="form" method="POST" action="{{ route('products.update', $product->id) }}" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="panel-body">
        <div class="row">        
            <div class="col-lg-12">
                <div class="margin-bottom-50">                    
                    <br />
                    <!-- Horizontal Form -->                                           
                    <input type="hidden" class="form-control" name="updated_by" id="updated_by" value="{{ Auth::user()->id }}">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Nama</label>
                        </div>
                        <div class="col-md-10">
                            <input id="name" type="text" class="form-control" name="name"  placeholder="Input nama user" value="{{$product->name}}" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Produk Tipe</label>
                        </div>
                        <div class="col-md-10">
                            <select id="product_type_id" type="text" class="form-control select2" name="product_type_id">
                                <option value="">--- Pilih Produk Tipe ---</option>
                                @foreach ($product_types as $product_type)
                                    @if($product_type->id == $product->product_type_id )
                                    <option value="{{ $product_type->id }}" selected >{{$product_type->name }}</option>
                                    @else
                                    <option value="{{ $product_type->id }}">{{$product_type->name }}</option>
                                    @endif
                                @endforeach
                            </select>

                            @if ($errors->has('product_type_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('product_type_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    @if($product->image)
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Gambar Post</label>
                        </div>
                        <div class="col-md-4">
                            <img id="image" src="/{{$product->image}}" alt="{{$product->image}}" style="width:100%;max-width:300px">                                
                        </div>
                        <div class="col-md-6">
                            <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image"  placeholder="Input Gambar Sidang" autofocus>

                            @if ($errors->has('image'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                            @endif
                        </div>                            
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-10">
                            <p class="text-red">* Upload jika ingin merubah gambar post</p>
                            <p class="text-red">* Ukuran Maksimal File 2MB</p>
                            <p class="text-red">* Format File : jpeg, jpg, png</p>
                        </div>
                    </div>                        
                    @else
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Gambar Post</label>
                        </div>
                        <div class="col-md-8">
                            <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image"  placeholder="Input Gambar Sidang" autofocus>

                            @if ($errors->has('image'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                            @endif
                        </div>                            
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4"></div>
                        <div class="col-xs-2">
                            <p class="text-red">* Ukuran Maksimal File 2MB</p>
                            <p class="text-red">* Format File : jpeg, jpg, png</p>
                        </div>
                    </div>
                    @endif
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Harga</label>
                        </div>
                        <div class="col-md-10">
                            <input id="price" type="text" class="form-control" name="price" value="{{$product->price}}"  placeholder="Input harga produk" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Deskripsi</label>
                        </div>
                        <div class="col-md-10">
                            <textarea id="description" name="description" class="form-control summernote" rows="4" cols="50" autofocus>
                            {{$product->description}}
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Jumlah</label>
                        </div>
                        <div class="col-md-10">
                            <input id="quantity" type="text" class="form-control" name="quantity" value="{{$product->quantity}}"  placeholder="Input Jumlah produk" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">link Tokped</label>
                        </div>
                        <div class="col-md-10">
                            <input id="link" type="text" class="form-control" name="link_tokped" value="{{$product->link_tokped}}" placeholder="Input Link tokped produk" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">link Shope</label>
                        </div>
                        <div class="col-md-10">
                            <input id="link" type="text" class="form-control" name="link_shope" value="{{$product->link_shope}}" placeholder="Input Link tokped produk" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">link Lazada</label>
                        </div>
                        <div class="col-md-10">
                            <input id="link" type="text" class="form-control" name="link_lazada" value="{{$product->link_lazada}}" placeholder="Input Link tokped produk" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">link Bukalapak</label>
                        </div>
                        <div class="col-md-10">
                            <input id="link" type="text" class="form-control" name="link_bukalapak" value="{{$product->link_bukalapak}}" placeholder="Input Link tokped produk" autofocus>
                        </div>
                    </div>
                </div>
            </div>
        </div>  

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Update') }}
                </button>
                <a class="btn btn-success" href="/dashboard/products/">Back</a>
            </div>
        </div>
    </div>
    </form>
    <!-- End Horizontal Form -->      
</section>

<!-- End -->
<script>
    $(function(){
        $('.select2').select2();

        $('.summernote').summernote({
            height: 350
        });

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD'
        });

        var image = $('#image');        
        var maxSize = image.data('max-size');
        var document = $('#document_0');
        $("form").submit(function(){
            if(image.val() != '' && image.get(0).files[0].size>maxSize){
                alert('file gambar sidang melebihi batas ' +maxSize+ ' bytes');
                return false;
            }else if(document.val() != '' && document.get(0).files[0].size>maxSize){
                alert('file dokumen repot melebihi batas ' +maxSize+ ' bytes');
                return false;
            }      
        });
    })
</script>
@endsection