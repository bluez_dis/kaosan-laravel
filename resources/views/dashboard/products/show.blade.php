@extends('layouts.dashboard')

@section('content')
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Form Data Post</h3>
    </div>
    <br />
    <form class="form-horizontal" role="form" method="POST" action="{{ route('products.update', $product->id) }}" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="panel-body">
        <div class="row">        
            <div class="col-lg-12">
                <div class="margin-bottom-50">                    
                    <br />
                    <!-- Horizontal Form -->                                           
                    <input type="hidden" class="form-control" name="updated_by" id="updated_by" value="{{ Auth::user()->id }}">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Nama Produk</label>
                        </div>
                        <div class="col-md-10">
                            <input id="name" type="text" class="form-control" name="name"  placeholder="Input Judul" value="{{$product->name}}" autofocus readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Tipe Produk</label>
                        </div>
                        <div class="col-md-10">                        
                        <input id="product_type_id" type="text" class="form-control" name="product_type_id"  placeholder="Input Tipe Product" value="{{ $product->product_type ? $product->product_type->name : '' }}" autofocus readonly>
                        </div>
                    </div>                    
                    @if($product->image)
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Gambar</label>
                        </div>
                        <div class="col-md-8">
                            <img id="image" src="/{{$product->image}}" alt="{{$product->image}}" style="width:100%;max-width:300px">                                
                        </div>                          
                    </div>                                          
                    @else                    
                    <div class="form-group row">
                        <div class="col-md-4"></div>
                        <div class="col-xs-2">
                            <p class="text-red">Gambar  belum di upload</p>
                        </div>
                    </div>
                    @endif
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Deskripsi</label>
                        </div>
                        <div class="col-md-10">
                            <p>
                                {!!$product->description!!}
                            </p>                                
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Stok Produk</label>
                        </div>
                        <div class="col-md-10">
                        <h1>{{$product->quantity}}</h1>                        
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Status Produk</label>
                        </div>
                        <div class="col-md-10">
                        @if ($product->status == 1)
                            <p>
                                <h1>Aktif</h1>                                
                            </p> 
                        @else
                            <p>
                                <h1>Tidak Aktif</h1>                                
                            </p> 
                        @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Harga Produk</label>
                        </div>
                        <div class="col-md-10">
                        <h1>RP. {{ number_format($product->price, 2) }}</h1>                        
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">link Tokped</label>
                        </div>
                        <div class="col-md-10">
                            <input id="link" type="text" class="form-control" name="link_tokped" value="{{$product->link_tokped}}" placeholder="Input Link tokped produk" autofocus readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">link Shope</label>
                        </div>
                        <div class="col-md-10">
                            <input id="link" type="text" class="form-control" name="link_shope" value="{{$product->link_shope}}" placeholder="Input Link tokped produk" autofocus readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">link Lazada</label>
                        </div>
                        <div class="col-md-10">
                            <input id="link" type="text" class="form-control" name="link_lazada" value="{{$product->link_lazada}}" placeholder="Input Link tokped produk" autofocus readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">link Bukalapak</label>
                        </div>
                        <div class="col-md-10">
                            <input id="link" type="text" class="form-control" name="link_bukalapak" value="{{$product->link_bukalapak}}" placeholder="Input Link tokped produk" autofocus readonly>
                        </div>
                    </div>
                                                                                                                     
                </div>
            </div>
        </div>  

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <a class='btn btn-info' href='{{url('dashboard/products/'.$product->id.'/edit')}}'>Edit</a>
                <a class="btn btn-success" href="/dashboard/products">Back</a>
            </div>
        </div>
    </div>
    </form>
    <!-- End Horizontal Form -->      
</section>
<!-- End -->

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>List Gambar Produk</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                @if ($message = Session::get('success_document'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif
                <div class="col-md-4">
                    <a href="/dashboard/products/create_image/{{$product->id}}" class="btn btn-primary">
                        Tambah Gambar Produk
                    </a>
                </div>
                <div class="col-xs-12">&nbsp;</div>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <table class="table table-hover nowrap" id="example2" width="100%">
                            <thead>
                              <tr>
                                <th>Nama Dokumen</th>
                                <th>Action</th>
                              </tr>
                            <tbody>
                                @foreach($product->product_images as $product_image)
                                <tr>
                                    <td>
                                        @if($product_image->image != null)
                                        <img id="image" src="/{{$product_image->image}}" alt="{{$product_image->image}}" style="width:100%;max-width:300px">                                        
                                        @else
                                            File Tidak tersedia
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{url('/dashboard/products/delete_product_image/'.$product_image->id)}}" class="btn btn-danger btn-xs">
                                            Delete
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>        
</section>
<script>
    $(function(){
        $('.select2').select2();

        $('.summernote').summernote({
            height: 350
        });

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD'
        });

        var image = $('#image');        
        var maxSize = image.data('max-size');
        var document = $('#document_0');
        $("form").submit(function(){
            if(image.val() != '' && image.get(0).files[0].size>maxSize){
                alert('file gambar sidang melebihi batas ' +maxSize+ ' bytes');
                return false;
            }else if(document.val() != '' && document.get(0).files[0].size>maxSize){
                alert('file dokumen repot melebihi batas ' +maxSize+ ' bytes');
                return false;
            }      
        });
    })
</script>
@endsection