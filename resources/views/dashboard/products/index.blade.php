@extends('layouts.dashboard')

@section('content')
<section class="page-content">
<div class="page-content-inner">    
    <!--  -->
    <section class="panel">
        <div class="panel-heading">
            <h3>
                Data Produk
            </h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    <div class="col-md-4">
                        <a href="/dashboard/products/create" class="btn btn-primary">
                            Tambahkan Produk
                        </a>
                    </div>
                    
                    <div class="col-xs-12">&nbsp;</div>                    
                    <div class="margin-bottom-50">
                        <table class="table table-hover nowrap" id="example1" width="100%">
                            <thead>
                              <tr>                                
                                <th>Nama Produk</th>
                                <th width="15%">Jumlah</th>
                                <th width="15%">Status</th>
                                <th width="20%">Aksi</th>
                              </tr>
                            <tbody>
                                @foreach($products as $product)
                                <tr>
                                    <td>{{$product->name}}</td>
                                    <td>{{ $product->quantity}}</td>                                   
                                    <td>
                                    @if ($product->status == 1)
                                        <div class="btn btn-success btn-xs" role="alert">
                                            <center><strong>Aktif</strong></center>
                                        </div>
                                    @else                                        
                                        <div class="btn btn-danger btn-xs" role="alert">
                                            <center><strong>Tidak Aktif</strong></center>
                                        </div>
                                    @endif
                                    </td>                                   
                                    <td>                        
                                        @if ($product->status == 1)
                                            <a class='btn btn-danger btn-xs' href='{{url('dashboard/products/change_status/'.$product->id)}}'><i class="fa fa-close"></i></a> 
                                        @else
                                            <a class='btn btn-info btn-xs' href='{{url('dashboard/products/change_status/'.$product->id)}}'><i class="fa fa-check"></i></a> 
                                        @endif                                                    
                                        <a class='btn btn-success btn-xs' href='{{url('dashboard/products/'.$product->id)}}'>show</a> 
                                        <a class='btn btn-primary btn-xs' href='{{url('dashboard/products/'.$product->id.'/edit')}}'>Edit</a>
                                        <a class='btn btn-danger btn-xs open-confirm-hapus' onclick="confirm('Are you sure?')" href='{{url('dashboard/products/delete')}}/{{$product->id}}'>Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End  -->

</div>

<!-- Page Scripts -->
<script>
    $(function(){
        $('.datepicker').datetimepicker({
            format:  "YYYY-MM-DD"
        });
    });
</script>
<!-- End Page Scripts -->
</section>
@endsection