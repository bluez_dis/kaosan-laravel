@extends('layouts.application')

@section('content')

<div class="main">
<!--  -->
    <section class="module" id="alt-features">
        <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
            <h2 class="module-title font-alt">Basic Tshirt</h2>
            <div class="module-subtitle font-serif">A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</div>
            </div>
        </div>
        <div class="row">
        <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="alt-features-item">
                <div class="alt-features-icon"><span class="icon-tools-2"></span></div>
                <h3 class="alt-features-title font-alt">Spesification</h3>
                <ul class="icon-list">
                <li>170 GSM/ 24s/ 100% Cotton</li>                    
                <li>Shoulder to shoulder tape</li>                    
                <li>Pre-shrunk</li>                    
                <li>Double needle bottom hem</li>                    
                <li>All measurements taken in CENTIMETERS</li>                    
                </ul>                  
            </div>            
            </div>
            <div class="col-md-6 col-lg-6 hidden-xs hidden-sm">
            <div class="alt-services-image align-center"><img src="/image/basic_tshirt.png" alt="Feature Image"></div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="alt-features-item">
                <div class="alt-features-icon"><span class="icon-tools"></span></div>
                <h3 class="alt-features-title font-alt">Size S</h3>
                Width : 45-47
                <br>
                Length : 68-69
            </div>
            <div class="alt-features-item">
                <div class="alt-features-icon"><span class="icon-tools"></span></div>
                <h3 class="alt-features-title font-alt">Size M</h3>
                Width : 51-53
                <br>
                Length : 73-75
            </div>
            <div class="alt-features-item">
                <div class="alt-features-icon"><span class="icon-tools"></span></div>
                <h3 class="alt-features-title font-alt">Size L</h3>
                Width : 54-57
                <br>
                Length : 76-78
            </div>
            <div class="alt-features-item">
                <div class="alt-features-icon"><span class="icon-tools"></span></div>
                <h3 class="alt-features-title font-alt">Size XL</h3>
                Width : 58-61
                <br>
                Length : 78-81
            </div>
            <div class="alt-features-item">
                <div class="alt-features-icon"><span class="icon-tools"></span></div>
                <h3 class="alt-features-title font-alt">Size 2XL</h3>
                Width : 62-64
                <br>
                Length : 81-83
            </div>
            </div>
            
        </div>
        </div>
    </section>
<!--  -->
    <section class="module-small">
        <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
            <h2 class="module-title font-alt">Latest in clothing</h2>
            </div>
        </div>
        <div class="row multi-columns-row">
            @foreach($products as $product)
            <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="shop-item">
                @if($product->image)
                <div class="shop-item-image"><img src="/{{$product->image}}" alt="{{ $product->name }}"/>
                <div class="shop-item-detail">
                    <a href="/product/{{$product->id}}" class="btn btn-round btn-b"><span class="icon-basket">Beli Produk</span></a>
                </div>
                </div>
                @else
                <div class="shop-item-image"><img src="/frontend/assets/images/kaos_hitam.png" alt="Accessories Pack"/>
                <div class="shop-item-detail"><a class="btn btn-round btn-b"><span class="icon-basket">Beli Produk</span></a></div>
                </div>
                @endif
                <h4 class="shop-item-title font-alt"><a href="#">{{ $product->name }}</a></h4>RP. {{ number_format($product->price, 2) }}
            </div>
            </div>
            @endforeach            
        </div>
        <div class="row mt-30">
            <div class="col-sm-12 align-center"><a class="btn btn-b btn-round" href="#">See all products</a></div>
        </div>
        </div>
    </section>
    <section class="module module-video bg-dark-30" data-background="">
        <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
            <h2 class="module-title font-alt mb-0">Be inspired. Get ahead of trends.</h2>
            </div>
        </div>
        </div>
        <div class="video-player" data-property="{videoURL:'https://www.youtube.com/watch?v=EMy5krGcoOU', containment:'.module-video', startAt:0, mute:true, autoPlay:true, loop:true, opacity:1, showControls:false, showYTLogo:false, vol:25}"></div>
    </section>
    <section class="module">
        <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
            <h2 class="module-title font-alt">Exclusive products</h2>
            <div class="module-subtitle font-serif">The languages only differ in their grammar, their pronunciation and their most common words.</div>
            </div>
        </div>
        <div class="row">
            <div class="owl-carousel text-center" data-items="5" data-pagination="false" data-navigation="false">
            <div class="owl-item">
                <div class="col-sm-12">
                <div class="ex-product"><a href="#"><img src="/frontend/assets/images/shop/product-1.jpg" alt="Leather belt"/></a>
                    <h4 class="shop-item-title font-alt"><a href="#">Leather belt</a></h4>£12.00
                </div>
                </div>
            </div>
            <div class="owl-item">
                <div class="col-sm-12">
                <div class="ex-product"><a href="#"><img src="/frontend/assets/images/shop/product-2.jpg" alt="Derby shoes"/></a>
                    <h4 class="shop-item-title font-alt"><a href="#">Derby shoes</a></h4>£54.00
                </div>
                </div>
            </div>
            <div class="owl-item">
                <div class="col-sm-12">
                <div class="ex-product"><a href="#"><img src="/frontend/assets/images/shop/product-3.jpg" alt="Leather belt"/></a>
                    <h4 class="shop-item-title font-alt"><a href="#">Leather belt</a></h4>£19.00
                </div>
                </div>
            </div>
            <div class="owl-item">
                <div class="col-sm-12">
                <div class="ex-product"><a href="#"><img src="/frontend/assets/images/shop/product-4.jpg" alt="Leather belt"/></a>
                    <h4 class="shop-item-title font-alt"><a href="#">Leather belt</a></h4>£14.00
                </div>
                </div>
            </div>
            <div class="owl-item">
                <div class="col-sm-12">
                <div class="ex-product"><a href="#"><img src="/frontend/assets/images/shop/product-5.jpg" alt="Chelsea boots"/></a>
                    <h4 class="shop-item-title font-alt"><a href="#">Chelsea boots</a></h4>£44.00
                </div>
                </div>
            </div>
            <div class="owl-item">
                <div class="col-sm-12">
                <div class="ex-product"><a href="#"><img src="/frontend/assets/images/shop/product-6.jpg" alt="Leather belt"/></a>
                    <h4 class="shop-item-title font-alt"><a href="#">Leather belt</a></h4>£19.00
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </section>
    <hr class="divider-w">
    
    <hr class="divider-w">
    <section class="module-small">
        <div class="container">
        <div class="row client">
            <div class="owl-carousel text-center" data-items="6" data-pagination="false" data-navigation="false">
            <div class="owl-item">
                <div class="col-sm-12">
                <div class="client-logo"><img src="/frontend/assets/images/client-logo-dark-1.png" alt="Client Logo"/></div>
                </div>
            </div>
            <div class="owl-item">
                <div class="col-sm-12">
                <div class="client-logo"><img src="/frontend/assets/images/client-logo-dark-2.png" alt="Client Logo"/></div>
                </div>
            </div>
            <div class="owl-item">
                <div class="col-sm-12">
                <div class="client-logo"><img src="/frontend/assets/images/client-logo-dark-3.png" alt="Client Logo"/></div>
                </div>
            </div>
            <div class="owl-item">
                <div class="col-sm-12">
                <div class="client-logo"><img src="/frontend/assets/images/client-logo-dark-4.png" alt="Client Logo"/></div>
                </div>
            </div>
            <div class="owl-item">
                <div class="col-sm-12">
                <div class="client-logo"><img src="/frontend/assets/images/client-logo-dark-5.png" alt="Client Logo"/></div>
                </div>
            </div>
            <div class="owl-item">
                <div class="col-sm-12">
                <div class="client-logo"><img src="/frontend/assets/images/client-logo-dark-1.png" alt="Client Logo"/></div>
                </div>
            </div>
            <div class="owl-item">
                <div class="col-sm-12">
                <div class="client-logo"><img src="/frontend/assets/images/client-logo-dark-2.png" alt="Client Logo"/></div>
                </div>
            </div>
            <div class="owl-item">
                <div class="col-sm-12">
                <div class="client-logo"><img src="/frontend/assets/images/client-logo-dark-3.png" alt="Client Logo"/></div>
                </div>
            </div>
            <div class="owl-item">
                <div class="col-sm-12">
                <div class="client-logo"><img src="/frontend/assets/images/client-logo-dark-4.png" alt="Client Logo"/></div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </section>
    <div class="module-small bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                <div class="widget">
                    <h5 class="widget-title font-alt">About Lembayung</h5>
                    <p>.</p>
                    <p>Phone: +6281311072021</p>
                    <p>Email:<a href="#">32nuansalembayung@gmail.com</a></p>
                </div>
            </div>
            <div class="col-sm-3">
            <!-- <div class="widget">
                <h5 class="widget-title font-alt">Recent Comments</h5>
                <ul class="icon-list">
                <li>Maria on <a href="#">Designer Desk Essentials</a></li>
                <li>John on <a href="#">Realistic Business Card Mockup</a></li>
                <li>Andy on <a href="#">Eco bag Mockup</a></li>
                <li>Jack on <a href="#">Bottle Mockup</a></li>
                <li>Mark on <a href="#">Our trip to the Alps</a></li>
                </ul>
            </div> -->
            </div>
            <div class="col-sm-3">
            <!-- <div class="widget">
                <h5 class="widget-title font-alt">Blog Categories</h5>
                <ul class="icon-list">
                <li><a href="#">Photography - 7</a></li>
                <li><a href="#">Web Design - 3</a></li>
                <li><a href="#">Illustration - 12</a></li>
                <li><a href="#">Marketing - 1</a></li>
                <li><a href="#">Wordpress - 16</a></li>
                </ul>
            </div> -->
            </div>
            <div class="col-sm-3">
            <!-- <div class="widget">
                <h5 class="widget-title font-alt">Popular Posts</h5>
                <ul class="widget-posts">
                <li class="clearfix">
                    <div class="widget-posts-image"><a href="#"><img src="/frontend/assets/images/rp-1.jpg" alt="Post Thumbnail"/></a></div>
                    <div class="widget-posts-body">
                    <div class="widget-posts-title"><a href="#">Designer Desk Essentials</a></div>
                    <div class="widget-posts-meta">23 january</div>
                    </div>
                </li>
                <li class="clearfix">
                    <div class="widget-posts-image"><a href="#"><img src="/frontend/assets/images/rp-2.jpg" alt="Post Thumbnail"/></a></div>
                    <div class="widget-posts-body">
                    <div class="widget-posts-title"><a href="#">Realistic Business Card Mockup</a></div>
                    <div class="widget-posts-meta">15 February</div>
                    </div>
                </li>
                </ul>
            </div> -->
            </div>
        </div>
    </div>
</div>
@endsection