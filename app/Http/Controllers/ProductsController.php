<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use File;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Models\Product;
use App\Models\ProductType;
use App\Models\ProductImage;

class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();        
        return view("dashboard.products.index",compact(["products"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product_types = ProductType::all();
        return view("dashboard.products.create",compact(["product_types"]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        // dd($input);
        $random = Str::random(5);
        
        $product = new Product;                
        $product->name = $input['name'];
        $product->price = $input['price'];
        $product->product_type_id = $input['product_type_id'];
        $product->description = $input['description'];
        $product->quantity = $input['quantity'];
        $product->link_tokped = $input['link_tokped'];
        $product->link_shope = $input['link_shope'];
        $product->link_lazada = $input['link_lazada'];
        $product->link_bukalapak = $input['link_bukalapak'];
        $product->status = 1;
        
        if(isset($input['image'])){
            $files = $input['image'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/products/'; // The destination were you store the document.
                if(!(file_exists(public_path('/uploads/attachment/products/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $product->image = $destinationPath.'/'.$filename;
            }
        }
        $product->save();

        
        $i = 0;
        $count_looping = $input['count_looping_product_image'];
        for ($x = 0; $x < $count_looping; $x++) {                
            $product_image = new ProductImage;
            $product_image->product_id = $product->id;

            // upload image
            if(isset($input['document'][$i])){
                $files = $input['document'][$i];
                if ($files) {
                    $destinationPath    = 'uploads/attachment/product_images/'; // The destination were you store the document.
                    if(!(file_exists(public_path('/uploads/attachment/product_images/'))))
                    {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                    $mime_type          = $files->getMimeType(); // Gets this example image/png
                    $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                    $filename           = time().'-'.$filename; // random file name to replace original
                    $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                    $product_image->image = $destinationPath.'/'.$filename;
                }
            }
            $product_image->save();
            $i++;
        }
                
        \Session::flash('success','Data Produk berhasil dibuat');
        return redirect("dashboard/products/");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view("dashboard.products.show",compact(["product"]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $product_types = ProductType::all();
        return view("dashboard.products.edit",compact(["product", "product_types"]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $product = Product::find($id);
        $product->name = $input["name"];
        $product->price = $input['price'];
        $product->product_type_id = $input['product_type_id'];
        $product->description = $input['description'];
        $product->quantity = $input['quantity'];
        $product->link_tokped = $input['link_tokped'];
        $product->link_shope = $input['link_shope'];
        $product->link_lazada = $input['link_lazada'];
        $product->link_bukalapak = $input['link_bukalapak'];

        if ($request->file('image')) {
            if(!is_null($product->image)){
                unlink($product->image);                
            }
            $files = $input['image'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/products/'; // The destination were you store the document.
                if(!(file_exists(public_path('/uploads/attachment/products/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $product->image = $destinationPath.'/'.$filename;
            }            
        }
        $product->save();
        \Session::flash('success','Product berhasil di update');
        return redirect("/dashboard/products/");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if(!is_null($product->image)){
            unlink($product->image);                
        }
        
        if(!is_null($product->product_images)){                            
            foreach($product->product_images as $product_image){
                if(!is_null($product_image->image)){
                    // remove image
                    unlink($product_image->image);
                }
                $product_image->delete();
            }
        }
        
        $product->delete();
        \Session::flash('success','Product berhasil di dalete');

        return redirect("/dashboard/products");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change_status($id)
    {
        $product = Product::find($id);
        // dd($product);
        if($product->status == 1){
            $product->status = 0;
        }else{
            $product->status = 1;
        }
        $product->save();
        \Session::flash('success','Status Produk berhasil di rubah');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create_image($id)
    {
        $product = Product::find($id);        
        $product->save();

        return view("dashboard.products.create_image",compact(["product"]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_image(Request $request)
    {
        $input = $request->all();
        $random = Str::random(5);
        
        $i = 0;
        $count_looping = $input['count_looping_product_image'];
        for ($x = 0; $x < $count_looping; $x++) {                
            $product_image = new ProductImage;
            $product_image->product_id = $input['product_id'];

            // upload image
            if(isset($input['document'][$i])){
                $files = $input['document'][$i];
                if ($files) {
                    $destinationPath    = 'uploads/attachment/product_images/'; // The destination were you store the document.
                    if(!(file_exists(public_path('/uploads/attachment/product_images/'))))
                    {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                    $mime_type          = $files->getMimeType(); // Gets this example image/png
                    $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                    $filename           = time().'-'.$filename; // random file name to replace original
                    $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                    $product_image->image = $destinationPath.'/'.$filename;
                }
            }
            $product_image->save();
            $i++;
        }     
        
        \Session::flash('success','Data Produk berhasil di update');
        return redirect("dashboard/products/".$product_image->product_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete_product_image($id)
    {
        $product_image = ProductImage::find($id);
        if(!is_null($product_image->image)){
            // remove image
            unlink($product_image->image);
        }
        $product_image->delete();
        \Session::flash('success','Dokumen berhasil dihapus');
        return redirect("/dashboard/products/".$product_image->product_id);
    }

}
