<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function product_type()
    {
        return $this->belongsTo('App\Models\ProductType', 'product_type_id');
    }

    public function product_images()
    {
        return $this->hasMany('App\Models\ProductImage');
    }
}
